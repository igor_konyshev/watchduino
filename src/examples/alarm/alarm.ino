/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#include <watchcore.h>
#include <screen.h>
#include <notes.h>

#include <Adafruit_GFX.h> 
#include <Adafruit_PCD8544.h> 
#include <Time.h> 
#include <JeeLib.h> 


class AlarmTest: public Screen 
{
    public:
    void draw(){
        display.setCursor(8,HEIGHT/2);
        display.println("Alarm test");
    }
    void controller() {}
    void update(){}
    void enter() {
        alarm_active = true;
        alarm = now();
    }
    void exit() {
        alarm_active = false;
    }
};

Screen *custom_screens[] ={ 
    new AlarmTest()
};

// Old McDonals had a farm ....
const int alarm_melody[] = {  NOTE_G4, NOTE_G4, NOTE_G4, NOTE_D4, NOTE_E4, NOTE_E4, NOTE_D4 , NOTE_B4, NOTE_B4, NOTE_A4, NOTE_A4, NOTE_G4};
const int alarm_durations[] = { 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3};
const int ALARM_SIZE = 12;

int main(void) {
    return sysloop(custom_screens, alarm_melody, alarm_durations, ALARM_SIZE);
}