/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class TimeSet: public Screen 
{
    public:
    TimeSet(){}
    ~TimeSet(){}
    void draw() {
        display.drawRoundRect(10+20*(selectedTimeSet%3),selectedTimeSet<3?15:30,(selectedTimeSet==5)?30:18,18,2 , BLACK);
        
        //print alarm time
        display.setCursor(13,20);
        display.println(getDigit(hour())+':');
        display.setCursor(33,20); 
        display.println(getDigit(minute())+':' );
        display.setCursor(53,20); 
        display.println(getDigit(second() ) );
        
        display.setCursor(13,35);
        display.println(getDigit(day())+'/');
        display.setCursor(33,35); 
        display.println(getDigit(month())+'/' );
        display.setCursor(53,35); 
        display.println(year() );
    }
    
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedTimeSet = (selectedTimeSet+1) %6;
        else if (pressedButton(UP_PIN) || pressedButton(DOWN_PIN) ){
            int inc = pressedButton(UP_PIN)?1:-1;
               
            switch(selectedTimeSet){
                case 0: adjustTime(inc*60*60);  break;
                case 1: adjustTime(inc*60); break;
                case 2: adjustTime(inc); break;
                case 3: adjustTime(inc*SECONDS_IN_DAY); break;
                case 4: adjustTime(inc*SECONDS_IN_DAY*30);break;
                case 5: adjustTime(inc*SECONDS_IN_DAY*365);  break;
            } 
        }
    }
    void update(){}
    void enter() {}
    void exit() {}
};
