/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class Pong: public Screen {
  
    private:
      int playerY ;
      int comY ;
      int ballY ;
      int ballX ;
      int ballDirX ;//1 if UP, -1 if DOWN
      int ballDirY ;//1 if RIGHT , -1 if LEFT
      int ballSpeed ;
      int playerScore;
      int comScore ;
    public:
    Pong(){
        refresh = 50;
        
      playerY = 30;
      comY = 30;
      ballY = 30;
      ballX = 42;
      ballDirX = 1;//1 if UP, -1 if DOWN
      ballDirY = 1;//1 if RIGHT , -1 if LEFT
      ballSpeed = 1;
      playerScore =0;
      comScore = 0;
    }
    void draw(){
    
        //playfield
        display.drawFastHLine(0, 8, 84, BLACK);
        display.drawFastVLine(42, 8, 40, BLACK);
        
        //players
        display.fillRect(0,playerY,2,10,BLACK);
        display.fillRect(82,comY,2,10,BLACK);
    
        //ball
        display.fillRect(ballX,ballY,3,3,BLACK);
        
        //score
        display.setCursor(42-15,10); 
        display.println(playerScore);
        display.setCursor(42+5,10); 
        display.println(comScore);
    }

    void controller(){
        if (pushedButton(SELECT_PIN)) ballSpeed = (ballSpeed %3 )+1 ;
        if (pressedButton(UP_PIN))  playerY ++;
        if (pressedButton(DOWN_PIN))playerY --;
    }
    void update(){
        int diffX = ballDirX * ballSpeed;
        int diffY = ballDirY* ballSpeed;
        ballX += diffX;
        ballY += diffY;
        
        if (ballX +2 > WIDTH) {
            gameSound();
            if (ballY - diffY < comY || ballY - diffY > comY + 10)  {
                ballX = WIDTH/2;
                ballY = HEIGHT/2;
                playerScore++;
            } else {
                ballDirX = ballDirX * -1;   
            }
        }  
        if (ballX < 0) {
            
            gameSound();
            if (ballY - diffY < playerY || ballY - diffY > playerY + 10) {
                ballX = WIDTH/2;
                ballY = HEIGHT/2;
                comScore++;
            } else {
                ballDirX = ballDirX * -1;   
            }
        } 
        
        if (ballY +2 > HEIGHT || ballY < 10 ){
            gameSound();
            ballDirY = ballDirY * -1;   
        }
        
        //com AI
        if (ballX > WIDTH/2) comY += (ballY > comY+5)?1:-1;
    }
    void enter() {}
    void exit() {}
};