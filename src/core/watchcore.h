/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#ifndef WATCHDUINO_CORE_H 
#define WATCHDUINO_CORE_H

#include <Adafruit_GFX.h> 
#include <Adafruit_PCD8544.h> 
#include "screen.h"

//STRING CONSTANTS

//CONTROLS
const int UP_PIN = 9;
const int DOWN_PIN = 3;
const int SELECT_PIN = 10;
const int CUSTOM_PIN = 12;

//SCREEN
extern Adafruit_PCD8544 display;

//SETTINGS 
extern bool sleep;
extern bool sound;
extern int selectedSettings;
extern int fixTime; //add this ammount of time every day at 3:00AM

//ALARM SET
extern bool alarm_active ; 
extern int alarm_mode ; 
extern long alarm ; 
extern int selectedAlarm ;
extern int playingAlarm ;

//PONG
extern int WIDTH;
extern int HEIGHT;

//TIMESET
extern long SECONDS_IN_DAY;
extern int selectedTimeSet;

void gameSound();
int main(void) ;
void checkSleep();
void controller(Screen *screens[], int sizeof_screens);
void wakeUp();
void fixDelay();
void buzzer(int times);
void printPowerDown();
void checkAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE);
bool isAlarmSounding();
void printStatusBar();
void printBarDate();
void printBarTime();
void printBarBattery();
void printBarAlarm();
void  playAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE);
void debug(String s );
void debug(int s );
int sysloop(Screen *user_screens[], const int alarm_melody[], const int alarm_durations[], const int ALARM_SIZE);

#endif