/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#ifndef WATCHDUINO_SETTINGS_H 
#define WATCHDUINO_SETTINGS_H

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class Settings: public Screen
{
    void draw(){
        display.drawRoundRect(0,10+10*selectedSettings,35,11,2 , BLACK);

        display.setCursor(2,12); display.println(F("Sleep"));
        display.setCursor(40,12); 
        if (sleep) display.println(F("yes"));
        else display.println(F("no"));

        display.setCursor(2,22);  display.println(F("Sound"));
        display.setCursor(40,22); 
        if (sound) display.println(F("yes"));
        else display.println(F("no"));

        display.setCursor(2,32);  display.println(F("Gap"));
        display.setCursor(40,32); display.println(String(fixTime)+'s');
    }
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedSettings = (selectedSettings+1) %3;
        if (pushedButton(UP_PIN)) switch(selectedSettings){
            case 0: sleep = !sleep; break;
            case 1: sound = !sound ;break;
        } 
        if (pushedButton(DOWN_PIN)) switch(selectedSettings){
            case 0: sleep = !sleep; break;
            case 1: sound = !sound ; break;
        }   
        
        if (pressedButton(UP_PIN)) switch(selectedSettings){
            case 2: fixTime++; break;
        } 
        if (pressedButton(DOWN_PIN)) switch(selectedSettings){
            case 2: fixTime--; break;
        }   
    }
    void update(){ }
    void enter(){}
    void exit(){}
};

#endif