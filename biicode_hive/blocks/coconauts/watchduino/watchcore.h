/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#include "coconauts/arduino/wstring.h"
#include "adafruit/gfx_library/adafruit_gfx.h"
#include "coconauts/adafruit_pcd8544/adafruit_pcd8544.h"
#include "coconauts/watchduino/screen.h"


//STRING CONSTANTS
const String YES_MSG = "Yes";
const String NO_MSG = "No";

//CONTROLS
const int UP_PIN = 9;
const int DOWN_PIN = 3;
const int SELECT_PIN = 10;
const int CUSTOM_PIN = 12;

//SCREEN
const int WIDTH= 84;
const int HEIGHT= 48;
extern int contrast;
extern Adafruit_PCD8544 display;

//SETTINGS 
extern bool sleep;
extern bool sound;
extern int selectedSettings;

//FIX TIME
extern int fixTime; //add this ammount of time every day at 3:00AM

//ALARM 
extern bool alarm_active; // 0 = inactive  
extern int alarm_mode; // 0 = hour_alarm (ej: alarm at 5:52)
                   // 1 = remaining_alarm / timer (ej: alarm in 5 minutes)
extern long alarm; //time in seconds

void gameSound();
int main(void) ;
void checkSleep();
void controller(Screen *screens[], int sizeof_screens);
void wakeUp();
void fixDelay();
void updateAlarmMode();
void buzzer(int times);
void printPowerDown();
void checkAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE);
bool isAlarmSounding();
void printStatusBar();
void printBarDate();
void printBarTime();
void printBarBattery();
void printBarAlarm();
void  playAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE);
void debug(String s );
void debug(int s );
long readVcc() ;
int sysloop(Screen *user_screens[], const int alarm_melody[], const int alarm_durations[], const int ALARM_SIZE);


