#This file is automatically created by biicode.
#Do not modify it, as your changes will be overwritten.


#Ordered list of libraries to link with
############### Defining Arduino variables adafruit_gfx_library ################

set(BII_TARGET_FILES_adafruit_gfx_library ../deps/adafruit/gfx_library/adafruit_gfx.cpp
			../deps/adafruit/gfx_library/adafruit_gfx.h
			../deps/adafruit/gfx_library/glcdfont.c)
set(BII_TARGET_adafruit_gfx_library_LIBS )

#Ordered list of libraries to link with
############### Defining Arduino variables coconauts_adafruit_pcd8544 ################

set(BII_TARGET_FILES_coconauts_adafruit_pcd8544 ../blocks/coconauts/adafruit_pcd8544/adafruit_pcd8544.cpp
			../blocks/coconauts/adafruit_pcd8544/adafruit_pcd8544.h)
set(BII_TARGET_coconauts_adafruit_pcd8544_LIBS adafruit_gfx_library)

#Ordered list of libraries to link with
############### Defining Arduino variables coconauts_arduino ################

set(BII_TARGET_FILES_coconauts_arduino ../blocks/coconauts/arduino/binary.h
			../blocks/coconauts/arduino/wstring.cpp
			../blocks/coconauts/arduino/wstring.h)
set(BII_TARGET_coconauts_arduino_LIBS )

#Ordered list of libraries to link with
############### Defining Arduino variables coconauts_jeelib ################

set(BII_TARGET_FILES_coconauts_jeelib ../blocks/coconauts/jeelib/jeelib.h
			../blocks/coconauts/jeelib/ports.cpp
			../blocks/coconauts/jeelib/ports.h
			../blocks/coconauts/jeelib/portsrf12.cpp
			../blocks/coconauts/jeelib/rf12.cpp
			../blocks/coconauts/jeelib/rf12.h)
set(BII_TARGET_coconauts_jeelib_LIBS coconauts_arduino)

#Ordered list of libraries to link with
############### Defining Arduino variables coconauts_time ################

set(BII_TARGET_FILES_coconauts_time ../blocks/coconauts/time/datestrings.cpp
			../blocks/coconauts/time/time.cpp
			../blocks/coconauts/time/time.h)
set(BII_TARGET_coconauts_time_LIBS )

# This is an auxiliary target, to collect common configuration
# for the executables in this block, but it is not a library
#Ordered list of libraries to link with
############### Defining Arduino variables coconauts/watchduino ################

set(BII_TARGET_FILES_coconauts/watchduino )
set(BII_TARGET_coconauts/watchduino_LIBS coconauts_adafruit_pcd8544 coconauts_jeelib coconauts_time adafruit_gfx_library coconauts_arduino)

#Ordered list of libraries to link with
############### Defining Arduino variables coconauts_watchduino_main ################

set(BII_TARGET_FILES_coconauts_watchduino_main ../blocks/coconauts/watchduino/images.cpp
			../blocks/coconauts/watchduino/images.h
			../blocks/coconauts/watchduino/main.cpp
			../blocks/coconauts/watchduino/notes.h
			../blocks/coconauts/watchduino/screen.h
			../blocks/coconauts/watchduino/utils.cpp
			../blocks/coconauts/watchduino/utils.h
			../blocks/coconauts/watchduino/watchcore.cpp
			../blocks/coconauts/watchduino/watchcore.h)
set(BII_TARGET_coconauts_watchduino_main_LIBS coconauts_adafruit_pcd8544 coconauts_jeelib coconauts_time adafruit_gfx_library coconauts_arduino)
