#This file is automatically created by biicode.
#Do not modify it, as your changes will be overwritten.


############### Defining Arduino functions adafruit_gfx_library ################


#====================================================================#
#                 Static library adafruit_gfx_library
#====================================================================#
generate_arduino_library(adafruit_gfx_library
    SRCS ${BII_TARGET_FILES_adafruit_gfx_library})



############### Defining Arduino functions coconauts_adafruit_pcd8544 ################


#====================================================================#
#                 Static library coconauts_adafruit_pcd8544
#====================================================================#
generate_arduino_library(coconauts_adafruit_pcd8544
    SRCS ${BII_TARGET_FILES_coconauts_adafruit_pcd8544})



############### Defining Arduino functions coconauts_arduino ################


#====================================================================#
#                 Static library coconauts_arduino
#====================================================================#
generate_arduino_library(coconauts_arduino
    SRCS ${BII_TARGET_FILES_coconauts_arduino})



############### Defining Arduino functions coconauts_jeelib ################


#====================================================================#
#                 Static library coconauts_jeelib
#====================================================================#
generate_arduino_library(coconauts_jeelib
    SRCS ${BII_TARGET_FILES_coconauts_jeelib})



############### Defining Arduino functions coconauts_time ################


#====================================================================#
#                 Static library coconauts_time
#====================================================================#
generate_arduino_library(coconauts_time
    SRCS ${BII_TARGET_FILES_coconauts_time})



# This block has executables, not building library

############### Defining Arduino functions coconauts_watchduino_main ################


#====================================================================#
#                     Firwmare coconauts_watchduino_main
#====================================================================#
generate_arduino_firmware(coconauts_watchduino_main
    SRCS ${BII_TARGET_FILES_coconauts_watchduino_main}
    LIBS ${BII_TARGET_coconauts_watchduino_main_LIBS}
    )


